import React, { Component } from "react";

class RobotForm extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <div>
        Name: <input id="name" type="text" />
        <br />
        Type: <input id="type" type="text" />
        <br />
        Mass: <input id="mass" type="text" />
        <br />
        <button
          value="add"
          onClick={() => {
            if (this.props.onAdd != null) {
              let r = {};
              r.name = document.getElementById("name").value;
              r.type = document.getElementById("type").value;
              r.mass = document.getElementById("mass").value;
              this.props.onAdd(r);
            }
          }}
        >
          {" "}
          Add{" "}
        </button>
      </div>
    );
  }
}

export default RobotForm;
